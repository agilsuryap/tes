import axios from "axios";
export default {
  // module.exports
  login(email, password) {
    return axios
      .post("https://relaone-api.herokuapp.com/auth/login", { email, password })
      
      .then(res => res.data);
  }

};

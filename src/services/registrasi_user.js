import axios from "axios";
export const registrasi_user = function (first_name, last_name, email, password, password_confirmation) {
    return axios
      .post("https://relaone-api.herokuapp.com/signup", { first_name, last_name, email, password, password_confirmation })
      .then(res => res.data);
  }


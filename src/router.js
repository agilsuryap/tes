import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import nama from './views/Login.vue'
import D_PesertaDiterima from './views/Dasboard/PendaftarDiterima.vue'
import EditProfile from './views/Dasboard/EditProfile.vue'



Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login1',
      name: 'login1',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Login1.vue')
    },
   
       {
      path: '/login',
      name: 'daftar_sekarang!',
      component: nama
    },
    {
      path: '/D_PesertaDiterima',
      name: 'Peserta_Diterima',
      component: D_PesertaDiterima
    },
    {
      path: '/EditProfile',
      name: 'Edit_Profile',
      component: EditProfile
    },


  ]
})
